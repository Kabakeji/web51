<div>
    <div class="row">
        <div class="col-xl-12">
            <img src="/images/websiteplanet-dummy-1450X850.png" class="img-fluid">
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-4 text-center mt-3">
                <img src="/images/img2.png" class="img-fluid">
                <p class="text-dark h3 mt-3">
                   Un cadre chaleureux
                </p>
                <p class="text-dark mt-3">
                    Notre équipe vous accueille pour vous faire déguster<br>
                    les meilleurs spécialités de la cuisine italienne.
                </p>
            </div>
            <div class="col-lg-4 text-center mt-3">
                <img src="/images/vin-1.png">
                <p class="text-dark h3 mt-3">
                    Carte de vins
                </p>
                <p class="text-dark mt-3">
                    La carte des vins est soigneusement sélectionnées<br>
                    parmi notre sommelier dont de nombreux vins italiens<br>
                    prestigieux
                </p>
            </div>
            <div class="col-lg-4 text-center mt-3">
                <img src="/images/img4.png">
                <p class="text-dark h3 mt-3">
                    Notre cuisine
                </p>
                <p class="text-dark mt-3">
                    La principale caractéristique de notre cuisine est la<br>
                    préparation de tous les plats en utilisant uniquement<br>
                    les ingrédients de saison les plus frais.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <hr class="border border-warning border-2">
                <hr class="border border-warning border-2">
            </div>
        </div>
        <div class="d-none d-lg-block">
            <div class="row mt-5">
                <div class="col-lg-4 text-center">
                    <div class="card border border-warning" style="border-radius: 0px;">
                        <div class="card-body">
                            <p class="text-dark h3">
                                « Festival de la truffe »
                            </p>
                        </div>
                        <div class="card-body">
                            <img src="/images/11-Tortelli.jpg" class="img-fluid">
                        </div>
                        <div class="card-body text-dark">
                            <p class="text-dark">
                                le plaisir d’une expérience gastronomique<br>
                                 unique !<br>
                                – jusqu’au 09 janvier 2021 –
                            </p>
                            <button class="btn btn-outline-secondary bg-secondary" style="border-radius:0px">
                                <a class=" nav-link text-white" href="#">
                                    Voir Ia carte 
                                </a>
                            </button>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-4 text-center">
                    <div class="card border border-warning" style="border-radius: 0px;">
                        <div class="card-body">
                            <p class="text-dark h3">
                                Carte Classique
                            </p>
                        </div>
                        <div class="card-body">
                            <img src="/images/02-800x600.jpg" class="img-fluid">
                        </div>
                        <div class="card-body text-dark">
                            <p class="text-dark">
                                Découvrez notre carte complète ainsi que<br>
                                les différentes formules proposées.
                            </p>
                            <button class="btn btn-outline-secondary bg-secondary mt-4" style="border-radius:0px">
                                <a class=" nav-link text-white" href="#">
                                    Voir Ia carte 
                                </a>
                            </button>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-4 text-center">
                    <div class="card border border-warning" style="border-radius: 0px;">
                        <div class="card-body">
                            <p class="text-dark h3">
                                Desserts & Grappa
                            </p>
                        </div>
                        <div class="card-body">
                            <img src="/images/1-Panna-Cotta.jpg" class="img-fluid">
                        </div>
                        <div class="card-body text-dark">
                            <p class="text-dark">
                               Déguster une variété de desserts gourmands<br>
                                pour finir votre repas en beauté.
                            </p>
                            <button class="btn btn-outline-secondary bg-secondary mt-4" style="border-radius:0px">
                                <a class=" nav-link text-white" href="#">
                                    Voir Ia carte 
                                </a>
                            </button>
                        </div>
                    </div>
                </div>  
             </div>
         </div>
        <div class="d-block d-lg-none">
            <div class="row mt-5">
                <div class="col-lg-4 text-center">
                    <div class="card border border-warning" style="border-radius: 0px;">
                        <div class="card-body">
                            <p class="text-dark h3">
                                « Festival de la truffe »
                            </p>
                        </div>
                        <div class="card-body">
                            <img src="/images/11-Tortelli.jpg" class="img-fluid">
                        </div>
                        <div class="card-body text-dark">
                            <p class="text-dark">
                                le plaisir d’une expérience gastronomique<br>
                                 unique !<br>
                                – jusqu’au 09 janvier 2021 –
                            </p>
                            <button class="btn btn-outline-secondary bg-secondary" style="border-radius:0px">
                                <a class=" nav-link text-white" href="#">
                                    Voir Ia carte 
                                </a>
                            </button>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-4 text-center mt-4">
                    <div class="card border border-warning" style="border-radius: 0px;">
                        <div class="card-body">
                            <p class="text-dark h3">
                                Carte Classique
                            </p>
                        </div>
                        <div class="card-body">
                            <img src="/images/02-800x600.jpg" class="img-fluid">
                        </div>
                        <div class="card-body text-dark">
                            <p class="text-dark">
                                Découvrez notre carte complète ainsi que<br>
                                les différentes formules proposées.
                            </p>
                            <button class="btn btn-outline-secondary bg-secondary mt-4" style="border-radius:0px">
                                <a class=" nav-link text-white" href="#">
                                    Voir Ia carte 
                                </a>
                            </button>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-4 text-center mt-4">
                    <div class="card border border-warning" style="border-radius: 0px;">
                        <div class="card-body">
                            <p class="text-dark h3">
                                Desserts & Grappa
                            </p>
                        </div>
                        <div class="card-body">
                            <img src="/images/1-Panna-Cotta.jpg" class="img-fluid">
                        </div>
                        <div class="card-body text-dark">
                            <p class="text-dark">
                               Déguster une variété de desserts gourmands<br>
                                pour finir votre repas en beauté.
                            </p>
                            <button class="btn btn-outline-secondary bg-secondary mt-4" style="border-radius:0px">
                                <a class=" nav-link text-white" href="#">
                                    Voir Ia carte 
                                </a>
                            </button>
                        </div>
                    </div>
                </div>  
             </div>
         </div>
         <div class="d-none d-lg-block">
             <div class="row mt-5">
                 <div class="col-lg-6 text-center">
                    <div class="card border border-warning" style="border-radius: 0px;">
                        <div class="card-body">
                            <p class="text-dark h3">
                                Click & Collect
                            </p>
                        </div>
                        <div class="card-body">
                            <img src="/images/takeaway_2-800x600.png" class="img-fluid">
                        </div>
                        <div class="card-body text-dark">
                            <p class="text-dark">
                               (Livraison à domicile par nos soins,<br>
                                ou retrait des plats au restaurant)
                            </p>
                            <button class="btn btn-outline-secondary bg-secondary mt-4" style="border-radius:0px">
                                <a class=" nav-link text-white" href="#">
                                    Voir Ia carte 
                                </a>
                            </button>
                        </div>
                    </div>   
                 </div>
                 <div class="col-lg-6 text-center">
                    <div class="card border border-warning" style="border-radius: 0px;">
                        <div class="card-body">
                            <p class="text-dark h3">
                                Livraison Wedely
                            </p>
                        </div>
                        <div class="card-body">
                            <img src="/images/logo-wedely-1-800x600.png" class="img-fluid">
                        </div>
                        <div class="card-body text-dark">
                            <p class="text-dark">
                               Uniquement pour le déjeuner<br>
                                (plats servis chauds) 
                            </p>
                            <button class="btn btn-outline-secondary bg-secondary mt-4" style="border-radius:0px">
                                <a class=" nav-link text-white" href="#">
                                    Voir Ia carte 
                                </a>
                            </button>
                        </div>
                    </div>   
                 </div>
             </div>
         </div>
         <div class="d-block d-lg-none">
             <div class="row mt-5">
                 <div class="col-lg-6 text-center">
                    <div class="card border border-warning" style="border-radius: 0px;">
                        <div class="card-body">
                            <p class="text-dark h3">
                                Click & Collect
                            </p>
                        </div>
                        <div class="card-body">
                            <img src="/images/takeaway_2-800x600.png" class="img-fluid">
                        </div>
                        <div class="card-body text-dark">
                            <p class="text-dark">
                               (Livraison à domicile par nos soins,<br>
                                ou retrait des plats au restaurant)
                            </p>
                            <button class="btn btn-outline-secondary bg-secondary mt-4" style="border-radius:0px">
                                <a class=" nav-link text-white" href="#">
                                    Voir Ia carte 
                                </a>
                            </button>
                        </div>
                    </div>   
                 </div>
                 <div class="col-lg-6 text-center mt-4">
                    <div class="card border border-warning" style="border-radius: 0px;">
                        <div class="card-body">
                            <p class="text-dark h3">
                                Livraison Wedely
                            </p>
                        </div>
                        <div class="card-body">
                            <img src="/images/logo-wedely-1-800x600.png" class="img-fluid">
                        </div>
                        <div class="card-body text-dark">
                            <p class="text-dark">
                               Uniquement pour le déjeuner<br>
                                (plats servis chauds) 
                            </p>
                            <button class="btn btn-outline-secondary bg-secondary mt-4" style="border-radius:0px">
                                <a class=" nav-link text-white" href="#">
                                    Voir Ia carte 
                                </a>
                            </button>
                        </div>
                    </div>   
                 </div>
             </div>
         </div> 
        <div class="row mt-5">
            <div class="col-lg-12 text-center">
                <hr class="border border-warning border-2">
                <hr class="border border-warning border-2">
            </div>
        </div>
        <div class="d-none d-lg-block">
            <div class="row mt-5">
                <div class="col-lg-6 text-start">
                    <p class="text-dark h3">
                        RESTAURANT ROMA
                    </p>
                    <p class="text-dark mt-3">
                        Institution incontournable de la gastronomie italienne au Grand-Duché (définition<br> Gault&Millaut 2020 lors de nos 70 ans) .
                    </p>
                    <p class="text-dark mt-3">
                        Inauguré en 1950, nous avons le privilège d’avoir été le premier restaurant italien qui a<br>
                        ouvert ses portes au Grand-Duché de Luxembourg.
                    </p>
                    <p class="text-dark mt-3">
                         En intérieur ou en terrasse, la carte chic et légère vous invite à redécouvrir les<br> classiques de la cuisine italienne.
                    </p>
                    <p class="text-dark mt-3">
                        Nos festivals thématiques et la carte variée transforment chaque repas en un vrai<br> festin.
                    </p>
                    <p class="text-dark mt-3 ms-2">
                        <b>Le restaurant dispose d’une grande salle pouvant accueillir jusqu’à<br>
                        50 personnes, mais aussi 2 salons de 24 et 10 places, qui sont à<br>
                        disposition de la clientèle pour les déjeuners d’affaires au cours<br>
                        desquels il sera possible de joindre l’utile à l’agréable ou pour tout<br>
                        autre repas familial que souhaite organiser le convive.</b>
                    </p>
                    <button class="btn btn-outline-secondary bg-secondary mt-5">
                        <a href="#" class="nav-link text-white">
                            Le Roma en image
                        </a>
                    </button>
                </div>
                <div class="col-lg-6 text-center">
                    <img src="/images/logo-wedely-1-800x600.png" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="d-block d-lg-none">
            <div class="row mt-5">
                <div class="col-lg-12 text-start">
                    <p class="text-dark h3">
                        RESTAURANT ROMA
                    </p>
                    <p class="text-dark mt-3">
                        Institution incontournable de la gastronomie italienne au Grand-Duché (définition<br> Gault&Millaut 2020 lors de nos 70 ans) .
                    </p>
                    <p class="text-dark mt-3">
                        Inauguré en 1950, nous avons le privilège d’avoir été le premier restaurant italien qui a<br>
                        ouvert ses portes au Grand-Duché de Luxembourg.
                    </p>
                    <p class="text-dark mt-3">
                         En intérieur ou en terrasse, la carte chic et légère vous invite à redécouvrir les<br> classiques de la cuisine italienne.
                    </p>
                    <p class="text-dark mt-3">
                        Nos festivals thématiques et la carte variée transforment chaque repas en un vrai<br> festin.
                    </p>
                    <p class="text-dark mt-3 ms-2">
                        <b>Le restaurant dispose d’une grande salle pouvant accueillir jusqu’à<br>
                        50 personnes, mais aussi 2 salons de 24 et 10 places, qui sont à<br>
                        disposition de la clientèle pour les déjeuners d’affaires au cours<br>
                        desquels il sera possible de joindre l’utile à l’agréable ou pour tout<br>
                        autre repas familial que souhaite organiser le convive.</b>
                    </p>
                    <button class="btn btn-outline-secondary bg-secondary mt-5">
                        <a href="#" class="nav-link text-white">
                            Le Roma en image
                        </a>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6 text-start">
                <img src="/images/websiteplanet-dummy-350X350.png" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark text-start h3">
                    Réservez votre table dès maintenant !
                </p>
                <p class="text-dark">
                    Vous souhaitez réserver une table dans notre restaurant ?<br>
                    Vous pouvez le faire via le formulaire ci-dessous, c’est facile et c’est rapide !
                </p>
                <button class="btn btn-outline-warning bg-warning"style="border-radius: 0px;">
                    <a class="nav-link text-white"> 
                        <i class="fas fa-bookmark"></i>
                        Reserver une table
                    </a>
                </button>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12 text-center">
                <p class="text-dark h5">
                    <b>AVIS CLIENTS</b>
                </p>
                <p class="text-dark h5 mt-3">
                    Partagez votre expérience : votre avis compte beaucoup pour nous !
                </p>
                <p class="text-dark h4 mt-5">
                    <b>Tablebooker Rating</b>
                </p>
                <p class="text-danger h1 mt-3">
                    <b>4.7</b>
                </p>
                <p class="text-danger h5">
                   <i class="fas fa-star"></i>
                   <i class="fas fa-star"></i>
                   <i class="fas fa-star"></i>
                   <i class="fas fa-star"></i>
                   <i class="fas fa-star-half-alt"></i> 
                </p>
            </div>
        </div>
    </div>
    <div class="bg-warning py-5 mt-5">
        <div class="row">
            <div class="col-lg-7 text-center">
                <p class="text-white h3">
                    Inscrivez vous à notre newsletter
                </p>
                <p class="text-white mt-4 text-center">
                    Restez à jour et retrouvez les nouveaux festivals, les suggestions du chef et les nouvelles carte.
                </p>
            </div>
            <div class="col-lg-5 text-start">
                <input type="" name="" class="bg-warning py-3 border border-secondary my-4">
                <button class="btn btn-outline-light bg-light"style="border-radius: 0px;">
                    <a href="#" class="nav-link text-warning h5">
                        S'nscrie
                    </a>
                </button>
            </div>
        </div>
    </div>
</div>
