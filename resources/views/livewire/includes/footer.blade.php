<div class="bg-dark">
    <div class="container">
        <div class="row ">
            <div class="col-lg-6 text-start">
                <p class="text-white h3 mt-5">
                    À propos de nous
                </p>
                <p class="text-white mt-3 h5">
                    Inauguré en 1950, nous avons le privilège d’avoir été<br>
                    le premier restaurant italien qui a ouvert ses portes<br>
                    au Grand-Duché de Luxembourg.
                </p>
            </div>
            <div class="col-lg-6 text-start">
                <p class="text-white h3 mt-5">
                    Contactez nous
                </p>
                <p class="text-white mt-3">
                    <i class="far fa-alarm-clock text-warning"></i>
                    5, Rue Louvigny L-1946 - LUXEMBOURG
                </p>
                <p class="text-white mt-3">
                    <i class="fas fa-phone-alt text-warning"></i>
                    +352 22 36 92
                </p>
                <p class="text-white mt-3">
                    <i class="fas fa-clock text-warning"></i>
                    Du mardi au Samedi: 12h-14h / 19h-22h<br>
                    Fermé le lundi et dimanche soir
                </p>
            </div>
        </div>
    </div>
</div>
